# Tu passeras jamais de Docker Compose à Kubernetes

Cf. https://redwatch.io/blog/2024/docker-compose-kubernetes/

Pour tester Kompose, prendre l'une des piles d'exemple fournies par Docker Compose :
```sh
wget https://raw.githubusercontent.com/docker/awesome-compose/fa1788d822147c17c5ce1f1c53e3230653cba1fe/wordpress-mysql/compose.yaml
```

Construire une image Kompose et lancer un conteneur de conversion :


```sh
docker build -t kompose https://github.com/kubernetes/kompose.git\#497809c81c65f8a9f883c5638e2fdf522223b76e
docker run --rm -it -v $PWD:/opt kompose sh -c "cd /opt && kompose convert"
```

Les fichiers produits sont statiques, ils peuvent être étudiés ou adaptés.

Supprimer l'image créée :

```sh
docker rmi -f kompose
```
