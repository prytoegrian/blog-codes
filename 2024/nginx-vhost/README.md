# Le vrai fonctionnement des vhosts de Nginx

Cf. https://redwatch.io/blog/2024/nginx-vhost/

Démarrer une pile initiale :

```sh
$ docker network create vhost
$ docker run -d -p 80:80 --name nginx-vhost --network vhost nginx:1-alpine
```

Par défaut, Nginx fournit un vhost ; on peut d'ors et déjà s'apercevoir qu'il répond à toute requête : 

```sh
$ curl app.localhost
[...]
<h1>Welcome to nginx!</h1>
[...]
```

> Il n'est pas nécessaire de configurer le `/etc/hosts`, la [RFC 2606](https://www.rfc-editor.org/rfc/rfc2606#section-2) route automatiquement le TLD localhost.

Relancer un conteneur avec deux vhosts :

```sh
$ docker rm -f nginx-vhost
$ docker run -d -p 80:80 --name nginx-vhost --network vhost -v ./conf.d:/etc/nginx/conf.d:ro -v ./app:/usr/share/nginx/html/app:ro nginx:1-alpine
```

Cette fois-ci, c'est un autre vhost qui attrape les requêtes inconnues car `default.conf` n'est plus le premier :

```sh
$ curl app.localhost
<h1>BAR</h1>
```

Faire du serveur par défaut le vrai serveur par défaut :

```sh
$ sed -i -r "s/listen(.*)80;/listen\180 default_server;/g" conf.d/default.conf
$ docker rm -f nginx-vhost
$ docker run -d -p 80:80 --name nginx-vhost --network vhost -v ./conf.d:/etc/nginx/conf.d:ro -v ./app:/usr/share/nginx/html/app:ro nginx:1-alpine
```

Relancer l'appel réseau :

```sh
$ curl app.localhost
[...]
<h1>Welcome to nginx!</h1>
[...]
```

Supprimer la pile :

```
$ docker rm -f nginx-vhost
$ docker rmi -f nginx:1-alpine
$ docker network remove vhost
```
