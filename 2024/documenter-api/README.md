# Comment documenter son API ReST au mieux

Cf. https://redwatch.io/blog/2024/documenter-api/

Swagger implémente OpenAPI pour offrir une Interface Utilisateur du fichier `openapi.yml` source et permettre à tout un chacun de manipuler l'API en mode graphique.

Démarrer la pile :
```sh
docker run -d --name swagger -p 8080:8080 -e SWAGGER_JSON=/etc/user/openapi.yml -v .:/etc/user swaggerapi/swagger-ui:v5.11.9
```

> NB: La variable d'environnement est mal nommée, l'appli supporte bien yaml


Dans le navigateur, saisir : `localhost:8080`. L'application affiche bien la spécification, mais notre exemple ne permet pas d'utiliser la fonction "Try it out".

Supprimer la pile :
```sh
docker rm -f swagger
docker rmi -f swaggerapi/swagger-ui:v5.11.9
```
