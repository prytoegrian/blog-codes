# Automatiser Portainer

Cf. https://redwatch.io/blog/2024/automatiser-portainer/

Démarrer la pile initiale :

```sh
$ docker network create portainer
$ docker volume create portainer_data
$ docker run -d -p 9443:9443 --network portainer --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data portainer/portainer-ce:2.19.4-alpine
$ docker build -t server .
```

Se connecter au serveur d'instrumentalisation :
```sh
$ docker run --rm -ti --network portainer server sh
```

Une fois connecté, vérifier que portainer répond :
```sh
$ curl -kI https://portainer:9443
```

Créer l'administrateur :
```sh
$ curl -X POST -k https://portainer:9443/api/users/admin/init -d '{"password": "Y55XGoAAey9PahKHAIvW", "username": "admin"}'
```

Générer un JWT dans un fichier pour le réutiliser :
```sh
$ curl -k -X POST https://portainer:9443/api/auth -d '{"password": "Y55XGoAAey9PahKHAIvW", "username": "admin"}' | jq -r .jwt | awk '{print "Authorization: Bearer "$1}' > headers
```

Créer un premier environnement :
```sh
$ curl -ki -H @headers https://portainer:9443/api/endpoints -F Name=local -F EndpointCreationType=1
```

Créer la pile à partir du fichier `stack.json`:
```sh
$ curl -ki -H @headers -H 'Content-Type: application/json' https://portainer:9443/api/stacks/create/standalone/repository?endpointId=1 --data @stack.json
```

Lister les piles :
```sh
$ curl -k -H @headers https://portainer:9443/api/stacks | jq '[.[] | with_entries(select(.key | in({"Id":1, "Name":1, "Env":1})))]'
```

Éditer les variables d'environnement de la pile :
```sh
$ curl -k -H @headers https://portainer:9443/api/stacks/1/git?endpointId=1 -d '{"env":[{"name": "mavar", "value": "mavaleur"}]}'
$ curl -k -H @headers https://portainer:9443/api/stacks | jq '[.[] | with_entries(select(.key | in({"Id":1, "Name":1, "Env":1})))]'
```

Redéployer la pile :
```sh
$ curl -ki -X PUT -H @headers -H 'Content-Type: application/json' https://portainer:9443/api/stacks/1/git/redeploy?endpointId=1 -d '{}'
```

Supprimer la pile :
```sh
$ curl -ki -X DELETE -H @headers https://portainer:9443/api/stacks/1?endpointId=1
```

Sortir du serveur et supprimer la stack :
```sh
$ exit
$ docker rm -f portainer
$ docker rmi -f portainer/portainer-ce:2.19.4-alpine
$ docker volume remove portainer_data
$ docker rmi -f server
$ docker network remove portainer
```

