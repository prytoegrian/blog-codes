# Et si le module block Ansible n'était pas ce que tu croyais ?

Cf. https://redwatch.io/blog/2024/module-block-ansible/

Démarrer la pile :

```sh
$ docker network create block-ansible
$ docker build  -t ansible-client ./client
$ docker build -t ansible-server ./server
$ docker run -d --name ansible-client --network block-ansible ansible-client
$ docker run -d --name ansible-server --network block-ansible ansible-server
```

Se connecter au client :

```sh
$ docker exec -ti ansible-client bash
```

Exécuter le premier playbook :

```sh
$ ansible-playbook -i hosts playbook-1.yml
```

Il y a 4 opérations "changed", et surtout 0 en "skipped".

Exécuter ensuite le second playbook :

```sh
$ ansible-playbook -i hosts playbook-2.yml
```

L'opération de configuration a été "skipped". On peut aussi le constater en se connectant au server ansible-server.

Supprimer la pile :

```sh
$ docker rm -f ansible-server ansible-client
$ docker rmi -f ansible-server ansible-client
$ docker network remove block-ansible
```
