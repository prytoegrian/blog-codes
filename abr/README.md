# Le streaming adaptatif

Cf. https://redwatch.io/blog/2022/streaming-adaptatif-comment/

Démarrer la stack :

```sh
$ docker build -t "abr" .
$ docker run -d -p 80:80 --name abr abr
```

Ouvrir le navigateur à `localhost:80/index.html` et lancer la vidéo pour constater le changement de qualité.

Supprimer la stack :
```sh
$ docker rm -f abr && docker rmi -f abr
```

Présentation réalisée grâce à https://github.com/video-dev/hls.js/tree/v1.2.0
