const TICK_INTERVAL = 10000;

var video = document.getElementById('video');
var bandwidth = document.getElementById("bandwidth");
var resolution = document.getElementById("resolution");
var selector = document.getElementById("selector");
var rendition = document.getElementById("rendition");
var quality = new URLSearchParams(document.location.search).get("quality") ?? "mq";
var selector = document.getElementById("selector");

selector.addEventListener("change", function (e) {
    if (e.target.value == "") {
        return false;
    }
    document.location.href = "?quality=" + e.target.value;
});


rendition.innerText = quality;

if (Hls.isSupported()) {
    var hls = new Hls({
        debug: false,
    });
    hls.loadSource('/' + quality + '/x36xhzz/x36xhzz.m3u8');
    hls.attachMedia(video);
    var changeRendition;
    hls.on(Hls.Events.MEDIA_ATTACHED, function () {
        video.muted = true;
        video.play();
        hls.firstLevel = Math.floor(hls.levels.length / 2);
        hls.trigger(Hls.Events.BUFFER_RESET, undefined);
    });

    hls.on(Hls.Events.BUFFER_CREATED, function () {
        showRendition = setInterval(function () {
            var current = hls.levels[hls.currentLevel];
            bandwidth.innerText = Math.floor(current.attrs.BANDWIDTH / 1000);
            resolution.innerText = current.attrs.RESOLUTION;

        }, TICK_INTERVAL);
    });
}
else if (video.canPlayType('application/vnd.apple.mpegurl')) {
    video.src = '/' + quality + '/x36xhzz/x36xhzz.m3u8';
    video.addEventListener('canplay', function () {
        video.play();
    });
}
