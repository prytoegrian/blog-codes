# Liskov expliqué

Cf. https://redwatch.io/blog/2022/liskov_explique/

Démarrer la stack:
```sh
$ docker build -t liskov .
$ docker run --rm -ti -v ${PWD}:/app liskov sbt
```

Effectuer la compilation initiale :
```
sbt > run
```

Ouvrir un IDE pour faire hériter Carré de Rectangle :
1. importer FormeHeritee dans Main.scala,
2. manipuler les fonctions des classes,
3. compiler.

Procéder de façon similaire avec la classe Humain.

Sortir de SBT :
```
sbt > exit
```

Supprimer la stack :
```sh
$ docker rmi liskov
```
