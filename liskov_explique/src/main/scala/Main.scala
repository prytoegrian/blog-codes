import FormeHeritee._
import Personne.{Composee => PC} 
import Personne.{Separee => PS}

object Main extends App {
  displayForme()
  displayPersonneSeparee()
  displayPersonneComposee()

  def displayForme(): Unit = {
    var rec = new Rectangle(45, 36)
    var carre = new Carre(22)
    carre.setLargeur(15)
    carre.setLongueur(66)
    printf(
      "Ce rectangle a une longueur de %d et une largeur de %d.\n",
      rec.longueur(),
      rec.largeur()
    )
    printf(
      "Ce carré a un cote de %d.\n",
      carre.cote()
    )
  }

  def displayPersonneSeparee(): Unit = {
    var humain = new PS.Humain(PS.Couleur.Bleu)
    printf("Cette personne a les yeux %s.\n", humain.couleurYeux())
    var vairon = new PS.Vairon(PS.Couleur.Bleu, PS.Couleur.Marron)
    printf(
      "Cette personne a les yeux %s / %s.\n",
      vairon.couleurOeilGauche(),
      vairon.couleurOeilDroit()
    )

  }

  def displayPersonneComposee(): Unit = {
    var poly = new PC.MainPolydactile(6)
    var yeuxCommuns = new PC.YeuxCommuns(PC.Couleur.Rouge)
    var compose = new PC.Humain(yeuxCommuns, poly)
    printf(
      "Cette personne est composée d'yeux %s et de mains à %d doigts.\n",
      compose.couleurYeux(),
      compose.nombreDoigtsMain()
    )

  }
}
