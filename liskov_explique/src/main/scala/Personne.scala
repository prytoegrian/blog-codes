package Personne.Separee {
  object Couleur extends Enumeration {
    type Couleur = Value

    val Bleu = Value("Bleu")
    val Rouge = Value("Rouge")
    var Marron = Value("Marron")
  }

  class Humain(private var _couleurYeux: Couleur.Couleur) {
    def couleurYeux() = _couleurYeux
  }

  class Vairon(
      private var _couleurOeilGauche: Couleur.Couleur,
      private var _couleurOeilDroit: Couleur.Couleur
  ) {
    def couleurOeilGauche() = _couleurOeilGauche

    def couleurOeilDroit() = _couleurOeilDroit
  }
}

package Personne.Composee {
  // Yeux
  object Couleur extends Enumeration {
    type Couleur = Value

    val Bleu = Value("Bleu")
    val Rouge = Value("Rouge")
    var Marron = Value("Marron")
  }

  trait Yeux {
    def getCouleurs(): Array[Couleur.Couleur]
  }

  class YeuxCommuns(private var _couleurYeux: Couleur.Couleur) extends Yeux {
    def getCouleurs() = Array(_couleurYeux)
  }

  class YeuxVairons(
      private var _couleurOeilGauche: Couleur.Couleur,
      private var _couleurOeilDroit: Couleur.Couleur
  ) extends Yeux {
    def getCouleurs() = Array(_couleurOeilGauche, _couleurOeilDroit)
  }

  // Main
  trait Main {
    def nombreDoigts(): Int
  }

  class MainCommune extends Main {
    def nombreDoigts() = 5
  }

  class MainPolydactile(private var _nombreDoigts: Int) extends Main {
    def nombreDoigts() = _nombreDoigts
  }

  // Humain
  class Humain(private var _yeux: Yeux, private var _main: Main) {
    def couleurYeux(): String = {
      _yeux.getCouleurs().mkString(" / ")
    }

    def nombreDoigtsMain(): Int = {
      _main.nombreDoigts()
    }
  }
}
