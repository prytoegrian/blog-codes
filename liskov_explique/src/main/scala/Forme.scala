package FormeHeritee {
  class Rectangle(protected var _longueur: Int, protected var _largeur: Int) {
    def longueur(): Int = _longueur

    def largeur(): Int = _largeur

    def setLargeur(valeur: Int): Unit = {
      _largeur = valeur
    }

    def setLongueur(valeur: Int): Unit = {
      _longueur = valeur
    }
  }

  class Carre(protected var _cote: Int) extends Rectangle(_cote, _cote) {

    override def setLargeur(valeur: Int): Unit = {
      assume(_largeur == _longueur, "PRE: largeur et longueur sont différentes")
      super.setLargeur(valeur)
      super.setLongueur(valeur)
    } ensuring (_largeur == _longueur, "POST: largeur et longueur sont différentes")

    override def setLongueur(valeur: Int): Unit = {
      assume(_largeur == _longueur, "PRE: largeur et longueur sont différentes")
      super.setLargeur(valeur)
      super.setLongueur(valeur)
    } ensuring (_largeur == _longueur, "POST: largeur et longueur sont différentes")

    def cote(): Int = super.longueur()
  }
}

package FormeSeparee {
  trait Forme {
    def surface(): Int
  }

  class Rectangle(private var _longueur: Int, private var _largeur: Int)
      extends Forme {
    def longueur(): Int = _longueur

    def largeur(): Int = _largeur

    def setLargeur(valeur: Int): Unit = {
      _largeur = valeur
    }

    def setLongueur(valeur: Int): Unit = {
      _longueur = valeur
    }

    def surface(): Int = {
      _longueur * _largeur
    }
  }

  class Carre(private var _cote: Int) extends Forme {

    def cote(): Int = _cote

    def setCote(valeur: Int): Unit = {
      _cote = valeur
    }

    def surface(): Int = {
      _cote * _cote
    }
  }
}
