# Transfert de tablespace MySQL chiffré au repos

Cf. https://redwatch.io/blog/2022/mysql-transferer-table-chiffre/

Démarrer les stacks source et destination :

```sh
$ docker build -t mysql_source ./source
$ docker run -d -v shared_transfer:/tmp/shared_transfer --name mysql_source mysql_source
$ docker build -t mysql_destination ./destination
$ docker run -d -v shared_transfer:/tmp/shared_transfer --name mysql_destination mysql_destination
```

Constater le chiffrement :
```sh
$ docker exec -ti mysql_source bash
$ mysql -u root -ptoor madatabase
mysql> show create table utilisateurs
[...] ENCRYPTION='Y'
```

Migrer la base de données : 
```sh
mysql> flush table utilisateurs for export;
mysql> \! cp /var/lib/mysql/madatabase/utilisateurs.* /tmp/shared_transfer/
mysql> exit
$ exit
```

Importer la base de données : 
```
$ docker exec -ti mysql_destination bash
$ mysql -u root -ptoor madatabase 
mysql> CREATE TABLE `utilisateurs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `maj_at` varchar(50) NOT NULL,
  `creation_at` datetime DEFAULT NULL,
  `creditLimit` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ENCRYPTION='Y';
mysql> alter table utilisateurs discard tablespace;
mysql> \! cp /tmp/shared_transfer/* /var/lib/mysql/madatabase;
mysql> \! chown mysql: /var/lib/mysql/madatabase/utilisateurs.*
mysql> alter table utilisateurs import tablespace;
```

Supprimer les stacks :
```sh
$ docker rm -f mysql_source && docker rmi mysql_source
$ docker rm -f mysql_destination && docker rmi mysql_destination
```
