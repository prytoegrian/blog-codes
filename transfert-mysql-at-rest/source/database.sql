CREATE DATABASE IF NOT EXISTS madatabase;
use madatabase;

CREATE TABLE utilisateurs (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `maj_at` varchar(50) NOT NULL,
  `creation_at` datetime,
  `creditLimit` decimal(10,2) NOT NULL
) ENCRYPTION="Y";

INSERT IGNORE INTO utilisateurs VALUES
(null, "Huff", "Carolina", date_sub(now(),interval floor(rand()*10) day), now(), 9999.5),
(null, "Lindsey", "Genevieve", date_sub(now(),interval floor(rand()*10) day), now(), 741.2),
(null, "Stokes", "Zachariah", date_sub(now(),interval floor(rand()*10) day), now(), 32.30),
(null, "Oneill", "Ramon", date_sub(now(),interval floor(rand()*10) day), now(), 200.10),
(null, "Howard", "Remington", date_sub(now(),interval floor(rand()*10) day), now(), 66.15),
(null, "Herring", "Kyra", date_sub(now(),interval floor(rand()*10) day), now(), 25.25),
(null, "Hill", "Payten", date_sub(now(),interval floor(rand()*10) day), now(), 53),
(null, "Cordova", "Katrina", date_sub(now(),interval floor(rand()*10) day), now(), 7.98),
(null, "Lam", "Braedon", date_sub(now(),interval floor(rand()*10) day), now(), 81.17),
(null, "Hooper", "Rylie", date_sub(now(),interval floor(rand()*10) day), now(), 352.4),
(null, "Navarro", "Ayanna", date_sub(now(),interval floor(rand()*10) day), now(), 50.80),
(null, "Sweeney", "Raul", date_sub(now(),interval floor(rand()*10) day), now(), null)
;
