# Envsubst templating

Cf. https://redwatch.io/blog/2022/envsubst-templating/

Démarrer la stack :

```sh
$ docker build -t "envsubst" .
$ docker run --rm -ti -e MEMORY_LIMIT=2G envsubst
```

Générer entièrement la configuration :
```sh
$ cd /etc/test
$ envsubst < local.cfg.tmpl > local.cfg
```

Constater que seule la memory_limit a été remplacée :
```sh
$ cat local.cfg
```

Mais si on précise les variables à altérer, rien ne change :
```sh
$ envsubst '$NOSET' < local.cfg.tmpl > local-spec.cfg
$ cat local-spec.cfg
```

Supprimer la stack :
```sh
$ docker rmi envsubst
```
