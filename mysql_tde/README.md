# MySQL Transparent Data Encryption

Cf. https://redwatch.io/blog/2022/mysql-chiffrement-at-rest/

Démarrer la stack :

```sh
$ docker build -t mysql_tde .
$ docker run -d --name mysql_tde mysql_tde
```

Observer la base de données:

```sh
$ docker exec -ti mysql_tde bash
$ mysql -u root -ptoor madatabase
mysql> show tables;
mysql> select * from utilisateurs_enc;
mysql> exit
```

Simuler un piratage :
```sh
$ cp /var/lib/mysql/madatabase/utilisateurs_enc.ibd /tmp/
$ mysql -u root -ptoor madatabase
mysql> drop table utilisateurs_enc;
mysql> exit
```

Reconstruire la table :
```sh
$ ibd2sdi ./utilisateurs_enc.ibd
[ERROR] ibd2sdi: Page [page id: space=3, page number=3] is corrupted. Checksum verification failed.
[ERROR] ibd2sdi: Couldn't read page 3.
[ERROR] ibd2sdi: Couldn't reach upto level zero.

```

Supprimer la stack :
```sh
$ docker rm -f mysql_tde && docker rmi mysql_tde
```
