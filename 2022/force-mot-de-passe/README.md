# Force d'un mot de passe

Cf. https://redwatch.io/blog/2022/force-mot-de-passe/

Tout technique de sécurité mise à part, « casser un mot de passe » s'apparente à le chercher dans une liste. Le script d'essai va donc effectuer une attaque par dictionnaire sur le mot de passe saisi.

Démarrer la stack :

```sh
$ docker build -t force-mdp .
$ docker run -ti --rm force-mdp bash
```

Simuler une attaque par dictionnaire sur votre mot de passe :

```sh
$ ./search.sh
[motdepasse]
```

Avant d'exécuter la recherche, le script vous affiche un score de sécurité. Le score est sans unité de 0 <= x <= 100, 50 commence à être sécurisé. À but de test, la restriction par dictionnaire est coupée.

Supprimer la stack :

```sh
$ docker rmi -f force-mdp
```


Il peut être intéressant d'aller piocher dans les listes https://weakpass.com/ pour aller plus loin dans les recherches.
