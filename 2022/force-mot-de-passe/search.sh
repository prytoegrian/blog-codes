#!/bin/bash

_compare_password_file() {
  local filename=${1}
  if [ ! -f "$filename" ]; then
    echo "Erreur : le fichier ${filename} n'existe pas."
    exit 1
  fi

  echo -n "$(basename ${filename})..."

  while read line; do
    if [[ "${password}" == "${line}" ]]; then
      echo " > ${password}"
      exit
    fi
  done < "${filename}"
  echo
}

_list_files() {
  local files=$(ls $1 | sort -R)
  echo
  echo "- ${1}/"

  for file in ${files}; do
    local full_file=${1}/${file}
    if [ -d "${full_file}" ]; then
      _list_files ${full_file}
	    continue
    fi
    if [[ ${full_file} == *".txt" ]]; then
      _compare_password_file "${full_file}"
    fi
  done
}

read -p "Saisissez un mot de passe : " password

echo "------"
echo "Score "
echo "------"
echo "${password}" | pwscore
echo

echo "---------------------------------------------"
echo "Comparaison du mot de passe aux dictionnaires"
echo "---------------------------------------------"

_list_files "$(readlink -f "Passwords")"
_list_files "$(readlink -f "Miscellaneous")"
