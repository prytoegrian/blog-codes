package recursivite

import scala.annotation.tailrec

object Main extends App {
  def factorialRecursive(n: Int): BigInt = {
    if (n < 0) {
      throw new IllegalArgumentException("n < 0")
    }
    if (n <= 1) {
      return 1
    }
    n * factorialRecursive(n-1)
  }

  def factorialIterative(n: Int): BigInt = {
    if (n < 0) {
      throw new IllegalArgumentException("n < 0")
    }
    var acc: BigInt = 1

    for (i <- 1 to n) {
      acc *= i
    }

    acc
  }

  def factorialTailRecursive(n: Int): BigInt = {
    if (n < 0) {
      throw new IllegalArgumentException("n < 0")
    }
    @tailrec
    def inner(n: Int, acc: BigInt): BigInt = {
      if (n <= 1) {
        return acc
      }
      inner(n-1, n*acc)
    }

    inner(n, 1)
  }

  // Main
  val SIZE = 1500
  println("Factorial recursive:", factorialRecursive(SIZE))
  //println("Factorial iterative:", factorialIterative(SIZE))
  //println("Factorial tail:", factorialTailRecursive(SIZE))
}
