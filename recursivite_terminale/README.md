# La récursivité terminale

Cf. https://redwatch.io/blog/2022/recursivite-terminale/

Démarrer la stack :

```sh
$ docker build -t recursivite .
$ docker run -ti --rm -v ${PWD}:/app --memory="1g" recursivite sbt
```

Effectuer la compilation initiale :
```
sbt > run 
```

Constater que les fonctions ne diffèrent que par leur algorithmique.
Dé/commenter pour exécuter les différentes fonctions.

Sortir de SBT :
```
sbt > exit
```

Supprimer la stack :
```sh
$ docker rmi recursivite
```
