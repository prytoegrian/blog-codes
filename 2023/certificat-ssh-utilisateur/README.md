# Certificats SSH d'utilisateurs

Cf. https://redwatch.io/blog/2023/certificat-ssh-utilisateur/

Démarrer la stack :

```sh
$ docker network create ssh-cert
$ docker build -f ./ca/Dockerfile -t user-ca-ssh .
$ docker build -f ./client/Dockerfile -t client-ssh .
$ docker run -d -h user-ca-ssh --name user-ca-ssh --network ssh-cert user-ca-ssh
$ docker run -d -h client-ssh --name client-ssh --network ssh-cert client-ssh
```

Se connecter au client et créer une clé pour notre utilisateur :

```sh
$ docker exec -ti client-ssh bash
$ cd ~/.ssh/
$ ssh-keygen -f rsa_root
$ exit
```

Pour « simuler » la publication de la clé publique à signer, la déposer manuellement sur l'autorité :

```sh
$ docker cp client-ssh:/root/.ssh/rsa_root .
$ docker cp ./rsa_root user-ca-ssh:/tmp/
```

Se connecter au serveur pour créer la clé de signature :

```sh
$ docker exec -ti user-ca-ssh bash
$ mkdir ~/.ssh/authority && cd ~/.ssh/authority
$ ssh-keygen -f user_ca -C "users authentication"
```

Toujours dans le serveur, signer la clé publiée :

```sh
$ cd /tmp
$ ssh-keygen -s ~/.ssh/authority/user_ca -I 'CA' -n root -V +60m ./rsa_root.pub
```

Le serveur n'a pas besoin d'être redémarré, il reconnaît déjà l'autorité de signature.

Sur l'hôte, déposer le certificat sur le client :

```sh
$ docker cp user-ca-ssh:/tmp/rsa_root-cert.pub .
$ docker cp ./rsa_root-cert.pub client-ssh:/root/.ssh/rsa_root-cert.pub
```

Sur le client, se connecter sur le serveur (il y aura un TOFU, il n'est pas important dans le contexte) :

```
$ ssh -o CertificateFile=/root/.ssh/rsa_root-cert.pub -i /root/.ssh/rsa_root user-ca-ssh
```

Supprimer la stack :

```sh
$ rm ./rsa_root*
$ docker rm -f user-ca-ssh client-ssh
$ docker rmi -f user-ca-ssh client-ssh
$ docker network remove ssh-cert
```
