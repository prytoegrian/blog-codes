# Résoudre un vhost dans un réseau docker

Cf. https://redwatch.io/blog/2023/docker-vhost/

Démarrer la stack :
```sh
$ docker network create vhost
$ docker build -t app ./app
$ docker run -d --name app --network vhost --network-alias redwatch.io app
$ docker build -t source ./source
```

Se connecter à la source :

```sh
$ docker run --rm -ti --network vhost source sh
```

Une fois connecté, on peut observer la résolution DNS via nslookup :
```
$ nslookup app
$ nslookup redwatch.io
```

Et constater que seul le vhost répond applicativement (comme attendu) :
```sh
$ curl app
$ curl redwatch.io
```

Sortir du client et supprimer la stack :
```sh
$ exit
$ docker rm -f app
$ docker rmi -f app source
$ docker network remove vhost
```

