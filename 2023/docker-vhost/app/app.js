'use strict';

const connect = require('connect')
const vhost = require('vhost')
const app = connect()

app.use(vhost('redwatch.io', function handle (req, res, next) {
  res.setHeader('Content-Type', 'text/plain')
  res.end('Hello world!')
}))

app.listen(80)

