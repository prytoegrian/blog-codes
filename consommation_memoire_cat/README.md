# La consommation mémoire de cat

Cf. https://redwatch.io/blog/2022/consommation_memoire_cat/

Pour cet exemple, on utilise debian comme image de base et non pas alpine. Cette dernière réimplémente en effet `cat` ; vous pouvez dailleurs comparer les différences.

Démarrer la stack :

```sh
$ docker build -t "cat" .
$ docker run -ti --rm --name cat cat
```

Dans le container, créer le fichier de travail :
```sh
$ dd if=/dev/urandom of=/tmp/1gib.test bs=1024 count=1048576
```

Étudier les appels systèmes:
```
$ strace -o /tmp/strace_cat cat /tmp/1gib.test > /dev/null 2>&1
$ grep "read\|write" /tmp/strace_cat
```

Supprimer la stack :
```sh
$ docker rmi cat
```
