# MySQL ibd

Cf. https://redwatch.io/blog/2022/mysql-fichier-ibd/

Démarrer la stack :

```sh
$ docker build -t mysql_ibd .
$ docker run -d --name mysql_ibd mysql_ibd
```

Observer la base de données:

```sh
$ docker exec -ti mysql_ibd bash
$ mysql -u root -ptoor madatabase
mysql> show tables;
mysql> select * from utilisateurs;
mysql> exit
```

Simuler un piratage :
```sh
$ cp /var/lib/mysql/madatabase/utilisateurs.ibd /tmp/
$ mysql -u root -ptoor madatabase
mysql> drop table utilisateurs;
mysql> exit
```

Reconstruire la table :
```sh
$ ibd2sdi /tmp/utilisateurs.ibd
mysql> create table utilisateurs (
    id int,
    nom varchar(50),
    prenom varchar(50),
    maj_at varchar(50),
    creation_at datetime,
    creditLimit decimal(10, 2)
);
mysql> alter table utilisateurs discard tablespace;
$ cp /tmp/utilisateurs.ibd /var/lib/mysql/madatabase/
$ chown mysql: /var/lib/mysql/madatabase/utilisateurs.ibd
mysql> alter table utilisateurs import tablespace;
```

Supprimer la stack :
```sh
$ docker rm -f mysql_ibd && docker rmi mysql_ibd
```
