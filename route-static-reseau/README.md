# Création de routage statique entre deux réseaux

Cf. https://redwatch.io/blog/2022/route-statique-reseau/

Pour cet exemple, on utilisera [Vagrant](https://www.vagrantup.com/docs/installation) à la place de Docker puisque ce dernier restreint par défaut les communications entre sous réseaux.

Démarrer la stack :
```sh
vagrant up
```

Constater l'impossibilité de la connexion entre web et backend :
```sh
$ vagrant ssh web1
web1> $ ping -c3 backend1
$ exit
```

Transformer routeur en routeur :
```sh
$ vagrant ssh router
router> $ sudo -s "echo 1 > proc/sys/net/ipv4/ip_forward"
router> $ sudo iptables -A FORWARD -j ACCEPT
router> $ sudo iptables -t nat -A POSTROUTING -s 192.168.150.0/24 -d 192.168.200.0/24 -j MASQUERADE
router> $ exit
```

Configurer le routage :
```sh
$ vagrant ssh web1
web1> $ ip route add 192.168.200.0/24 via 192.168.150.10
web1> $ ping -c3 backend1
web1> exit
```

Supprimer la stack :
```sh
vagrant destroy -f
```

---

Source :
- https://docs.docker.com/network/bridge/#enable-forwarding-from-docker-containers-to-the-outside-world
- https://stackoverflow.com/a/51373066
- https://www.cb-net.co.uk/devops/docker-container-network-isolation/
