# HAProxy 503

Cf. https://redwatch.io/blog/2022/haproxy-503/

Démarrer la stack :

```sh
$ docker build -t "haproxy_503" .
$ docker run -d -p 80:80 -p 8080:8080 haproxy_503
```

Ouvrir le navigateur à `localhost:8080/stats` pour constater qu'aucun serveur n'est en capacité de répondre au frontend principal.

Ouvrir le navigateur à `localhost:80` et tomber sur la page 503 personnalisée.


Supprimer la stack :
```sh
$ docker rmi haproxy_503
```
