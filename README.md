# Blog Codes

Chaque code associé à un article du blog devra suivre le fonctionnement suivant :
1. Création d'une branche portant le nom du billet,
2. Création d'un sous-répertoire avec l'année et le même nom,
3. Squash de la branche de travail.

Des instructions de fonctionnement particulières se trouveront dans le readme associé.

Icon temporaire : https://www.iconfinder.com/icons/7122464/code_icon
