#!/bin/sh

for i in a b c d
do
  mkdir -p /tmp/${i}
  current_rand=$(shuf -i 1-100 -n 1)
  for j in $( seq 1 ${current_rand} )
  do
    head /dev/urandom >> /tmp/${i}/file${current_rand}
  done
done
