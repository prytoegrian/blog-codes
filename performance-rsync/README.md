# La performance de rsync

Cf. https://redwatch.io/blog/2022/puissance-rsync/

Démarrer la stack :

```sh
$ docker network create rsync
$ docker build -f ./source/Dockerfile -t rsync-source .
$ docker build -f ./destination/Dockerfile -t rsync-destination .
$ docker run -d --name rsync-destination --network rsync rsync-destination
```

Se connecter à la source :

```sh
$ docker run --rm -ti --network rsync rsync-source sh 
```

Un certain nombre de fichiers sont déjà prêts, observer leur taille avec :
```sh
$ ls -l /tmp/*
```

Synchroniser les serveurs une première fois (le mot de passe est `toor`):

```sh
$ rsync -vap --stats /tmp/ root@rsync-destination:/tmp/
```

Constater que les valeurs de `total file size` et `literal data` correspondent à la somme des taille de fichiers (cf. http://www.lbackup.org/backup_statistics).

Rajouter des données dans un fichier et relancer la synchronisation :
```sh
$ head -c 1000 /dev/urandom >> /tmp/a/file{X}
$ rsync -vap --stats /tmp/ root@rsync-destination:/tmp/
```

Constater que seuls les derniers octets ont été transmis à `rsync-destination` et non le fichier entier (le supplément étant dû aux informations de reconstruction).

Sortir du client et supprimer la stack :

```sh
$ exit
$ docker rm rsync-destination
$ docker rmi rsync-source rsync-destination
$ docker network remove rsync
```

