# TOFU et certificats d'hôtes

Cf. https://redwatch.io/blog/2022/certificats_ssh_hosts/

Démarrer la stack :

```sh
$ docker network create ssh-cert
$ docker build -f ./ca/Dockerfile -t server-ca-ssh .
$ docker build -f ./client/Dockerfile -t client-ssh .
$ docker run -d -h server-ca-ssh --name server-ca-ssh --network ssh-cert server-ca-ssh
$ docker run -d -h client-ssh --name client-ssh --network ssh-cert client-ssh
```

Se connecter au client et observer le TOFU :

```sh
$ docker exec -ti client-ssh bash
$ ssh root@server-ca-ssh
```

Se connecter au serveur pour créer la clé de signature :

```sh
$ docker exec -ti server-ca-ssh bash
$ mkdir ~/.ssh/authority && cd ~/.ssh/authority
$ ssh-keygen -f server_ca
```

Toujours dans le serveur, signer une clé pré-existante :

```sh
$ ssh-keygen -s server_ca -I $(hostname) -h -n $(hostname) -V +1w /etc/ssh/ssh_host_rsa_key.pub
```

Comparer les empreintes au contenu du certificat :

```sh
$ ssh-keygen -lf server_ca.pub
$ ssh-keygen -lf /etc/ssh/ssh_host_rsa_key.pub
$ ssh-keygen -Lf /etc/ssh/ssh_host_rsa_key-cert.pub
```

Récupérer la clé publique de certification : 
```sh
cat server_ca.pub
```

Le serveur n'a pas besoin d'être redémarré, sa configuration contient déjà le path du certificat.

Sur le client, ajouter l'autorité de certification :

```sh
$ docker exec -ti client-ssh bash
echo "@cert-authority *-ssh [contenu de la clé]" > /etc/ssh/ssh_known_hosts
```

Se connecter à nouveau au serveur, avec le mot de passe toor :

```
$ ssh root@ca.redwatch.local
```

Supprimer la stack :

```sh
$ docker rm -f server-ca-ssh client-ssh
$ docker rmi -f server-ca-ssh client-ssh
$ docker network remove ssh-cert
```
